﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _01_dotnet_list
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string ImagePath { get; set; }
    }

    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Person> people = new ObservableCollection<Person>();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            listPerson.ItemsSource = people;
        }

        private void AddPersonBtn_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TextName.Text) || string.IsNullOrWhiteSpace(TextAge.Text))
            {
                MessageBox.Show("Fields cannot be blank!");
            }
            else
            {
                string pathToImage;
                if (TextImagePath.Text == "") pathToImage = "pack://application:,,,/default_photo.png";
                else pathToImage = TextImagePath.Text;
                people.Add(new Person { Name = TextName.Text, Age = int.Parse(TextAge.Text),
                                        ImagePath = pathToImage });
                TextName.Text = ""; TextAge.Text = ""; TextImagePath.Text = "";
                personImage.Source = new BitmapImage(new Uri("pack://application:,,,/default_photo.png"));
            }
        }

        private void BrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "All files|*.jpg;*.jpeg;*.png|JPEG Files (.jpeg)|*.jpeg|PNG files (.png)|*.png|JPG Files (.jpg)|*.jpg";

            Nullable<bool> openDialog = dlg.ShowDialog();

            if (openDialog == true)
            {
                string filename = dlg.FileName;
                TextImagePath.Text = filename;
                personImage.Source = new BitmapImage(new Uri(dlg.FileName));
            }
        }

        private void ClearListBtn_Click(object sender, RoutedEventArgs e)
        {
            listPerson.UnselectAll();
            people.Clear();
        }

        private void ListPerson_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listPerson.SelectedItem == null) return;
            else
            {
                Person selectedPerson = listPerson.SelectedItem as Person;
                SelectedIntro.Content = "Your current selection:";
                SelectedName.Content = selectedPerson.Name;
                SelectedAge.Content = selectedPerson.Age;
                var converter = new ImageSourceConverter();
                SelectedImage.Source = (ImageSource)converter.ConvertFromString(selectedPerson.ImagePath);
            }
        }
    }
}
